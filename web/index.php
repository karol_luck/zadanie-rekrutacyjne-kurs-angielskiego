<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

function __autoload($class) {
    if(file_exists('../app/class/'.$class.'.php')) {
        include_once('../app/class/'.$class.'.php');
    }
}
require('../app/controllers/Controller.php');

if(isset($_GET['c'])) $controllerName=$_GET['c'];
if(isset($_GET['a'])) $actionName=$_GET['a'];

if(!isset($controllerName)||!file_exists('../app/controllers/'.$controllerName.'Controller.php')) $controllerName='Page';
$className=$controllerName.'Controller';

require_once('../app/controllers/'.$className.'.php');
$controller=new $className;

if(!isset($actionName)||!method_exists($controller, $actionName.'Action')) $actionName='index';
$methodName=$actionName.'Action';

$controller->$methodName();
$controller->printView($controllerName, $actionName);