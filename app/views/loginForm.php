<form class="form-signin" role="form" method="post">
    <h2 class="form-signin-heading">Logowanie</h2>
    <input name="email" type="email" class="form-control" placeholder="E-mail" required autofocus>
    <input name="password" type="password" class="form-control" placeholder="Hasło" required>
    <br />
    <button class="btn btn-lg btn-def btn-block" type="submit">Zaloguj się</button>
</form>