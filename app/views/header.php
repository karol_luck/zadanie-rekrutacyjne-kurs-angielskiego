<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Kurs języka angielskiego</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="/bootstrap/css/blog.css" />
    <link rel="stylesheet" href="/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.js"></script>
    <script src="/js/jplayer.min.js"></script>
</head>
<body>

<div class="blog-masthead">
    <div class="container">
        <nav class="blog-nav">
            <a class="blog-nav-item <?php if($this->_controller=='Page'&&$this->_action=='index') echo 'active'; ?>" href="./">Strona główna</a>
            <a class="blog-nav-item <?php if($this->_controller=='Lesson'&&$this->_action=='index') echo 'active'; ?>" href="./?c=Lesson">Spis lekcji</a>
            <a class="blog-nav-item <?php if($this->_controller=='Lesson'&&$this->_action=='try') echo 'active'; ?>" href="./?c=Lesson&a=try">Lekcja próbna</a>
            <a class="blog-nav-item <?php if($this->_controller=='Page'&&$this->_action=='about') echo 'active'; ?>" href="./?a=about">O kursie</a>
        </nav>

    </div>
</div>
<div class="container main-container">