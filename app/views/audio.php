<div class="clearfix">
    <div id="jplayer_<?php echo $this->param_no; ?>"></div>
    <div id="jplayer_interface_<?php echo $this->param_no; ?>">
        <div class="jp-controls">
            <a href="javascript:;" class="jp-play jp-button jp-button-play" title="play"></a>
            <a href="javascript:;" class="jp-pause jp-button jp-button-pause" title="pause"></a>
            <a href="javascript:;" class="jp-stop jp-button jp-button-stop" title="stop"></a>
        </div>
        <div class="jp-progress">
            <div class="jp-seek-bar">
                <div class="jp-play-bar"></div>
            </div>
        </div>
        <div class="sound-controls">
            <a href="javascript:;" class="jp-mute jp-button jp-button-speaker" title="mute"></a>
            <a href="javascript:;" class="jp-unmute jp-button jp-button-muted" title="unmute"></a>
        </div>
        <div class="jp-volume-bar">
            <div class="jp-volume-bar-value"></div>
        </div>
    </div>

</div>

<script>
    $("#jplayer_<?php echo $this->param_no; ?>").jPlayer({
        swfPath: "http://www.jplayer.org/latest/js/Jplayer.swf",
        ready: function () {
            $(this).jPlayer("setMedia", { mp3: "<?php echo $this->param_url; ?>", });
        },
        play: function () { $(this).jPlayer("pauseOthers"); },
        cssSelectorAncestor:"#jplayer_interface_<?php echo $this->param_no; ?>",
        supplied: "mp3"
    });
</script>
