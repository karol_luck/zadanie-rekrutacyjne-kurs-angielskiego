<?php include('../app/views/header.php'); ?>

    <div class="page-header">
        <h1>Kurs języka angielskiego</h1>
        <p class="lead">Skorzystaj z naszej oferty kursu ONLINE. Bez wychodzenia z domu zdobądź praktyczne umiejętności komunikacyjne w języku angielskim.</p>
    </div>
    <h3>Kilka słów o tej stronie</h3>
    <p>Strona kursu powstała na potrzeby procesu rekrutacji. Jest to jedynie strona poglądowa - nie została stworzona w celach, jakie są opisane na stronie głównej. Korzystanie ze wszystkich funkcjonalności jest oparte na zasadzie "tak jak jest".</p>
    <p>Biblioteki wykorzystane do wykonania tej strony internetowej</p>
    <ul>
        <li>Twitter Bootstrap</li>
        <li>Dodatkowy styl "bootstrap blog" dostępny na stronie: http://getbootstrap.com/examples/blog/</li>
    </ul>
    <p>Wykorzystane zasoby multimedialne</p>
    <ul>
        <li>Darmowe grafiki pobrane ze strony: http://freepik.com</li>
        <li>Ikony wykorzystane w playerze: http://iconspedia.com</li>
        <li>Text to speech API ze strony: http://tts-api.com/</li>
    </ul>

<?php include('../app/views/footer.php'); ?>