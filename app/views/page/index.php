<?php include('../app/views/header.php'); ?>

<div class="page-header">
    <h1>Kurs języka angielskiego</h1>
    <p class="lead">Skorzystaj z naszej oferty kursu ONLINE. Bez wychodzenia z domu zdobądź praktyczne umiejętności komunikacyjne w języku angielskim.</p>
</div>
<div class="col-md-4">
    <img src="/images/london_calling.jpg" style="width: 100%;" />
</div>
<div class="col-md-7">
    <h3>Wygodnie i bezproblemowo!</h3>
    <p>Własnie tak wygląda nauka u nas. Uczysz się w wolnym czasie, popijając kawkę :)</p>
    <h3>Dostęp 24/7</h3>
    <p>Zaglądasz kiedy chcesz, materiały są dostępne ONLINE z każdego miejsca na świecie.</p>
    <h3>Nacisk na konwersacje</h3>
    <p>Na tym kursie nauczysz się między innymi porpawnej wymowy słów codziennego użytku.</p>
    <h3>Dużo ćwiczeń</h3>
    <p>Nauka nie może opierać się tylko na teorii. W ramach kursu twoje nabyte umiejętności zostaną niejednokrotnie przećwiczone.</p>
</div>

<?php include('../app/views/footer.php'); ?>