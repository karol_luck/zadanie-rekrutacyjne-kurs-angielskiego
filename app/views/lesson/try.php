<?php include('../app/views/header.php'); ?>

<script>
    function clearBlockAnswers(blockId) {
        $("#blok_"+blockId+" .answer").each(function(){
            $(this).removeClass('answerCorrect');
            $(this).removeClass('answerBad');
            $("#blok_"+blockId+" .blockResult").hide();
        });
    }
    function printResult(blockId, answers, goodAnswers) {
        var percent=Math.round(100*goodAnswers/answers);
        $("#blok_"+blockId+" .blockResult").html("Twój wynik: "+goodAnswers+" / "+answers+" ( "+percent+"% )");
        $("#blok_"+blockId+" .blockResult").fadeIn();
    }
</script>

<div class="page-header">
    <h1>Lekcja próbna: <?php echo $this->lesson->get('name'); ?></h1>
</div>
<div>
    <?php foreach($this->bloki AS $blok) { ?>
        <div id="blok_<?php echo $blok->getId(); ?>" class="blok">
            <?php
                echo $blok->getParsedValue();
                if($blok->hasAnswers()) { ?>
                    <button id="block_check_<?php echo $blok->getId(); ?>" type="button" class="btn btn-default"><span class="glyphicon glyphicon-ok"></span> Sprawdź</button>
                    <button id="block_show_<?php echo $blok->getId(); ?>" type="button" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Pokaż odpowiedzi</button>
                    <button id="block_del_<?php echo $blok->getId(); ?>" type="button" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span> Wyczyść odpowiedzi</button>
                <?php } ?>

            <script>
                $("#block_check_<?php echo $blok->getId(); ?>").click(function(){
                    var blockId=<?php echo $blok->getId(); ?>;
                    clearBlockAnswers(blockId);
                    $.ajax({
                        url: "/?c=Lesson&a=ajax&command=blockShow&block="+blockId,
                        cache: false
                    })
                        .done(function( html ) {
                            var i=0;
                            var goodAnswers=0;
                            var json= $.parseJSON(html);
                            $("#blok_"+blockId+" .answer").each(function(){
                                if($(this).val().toLowerCase()!=json[i++].toLowerCase()) {
                                    $(this).addClass('answerBad');
                                }
                                else {
                                    $(this).addClass('answerCorrect');
                                    goodAnswers++;
                                }
                            });
                            printResult(blockId, json.length, goodAnswers);
                        });
                });
                $("#block_show_<?php echo $blok->getId(); ?>").click(function(){
                    var blockId=<?php echo $blok->getId(); ?>;
                    clearBlockAnswers(blockId);
                    $.ajax({
                        url: "/?c=Lesson&a=ajax&command=blockShow&block="+blockId,
                        cache: false
                    })
                        .done(function( html ) {
                            var i=0;
                            var json= $.parseJSON(html);
                            $("#blok_"+blockId+" .answer").each(function(){
                                $(this).val(json[i++]);
                            });
                        });
                });
                $("#block_del_<?php echo $blok->getId(); ?>").click(function(){
                    var blockId=<?php echo $blok->getId(); ?>;
                    clearBlockAnswers(blockId);
                    $("#blok_"+blockId+" .answer").each(function(){
                        $(this).val('');
                    });
                });
            </script>
            <div class="blockResult" style="display: none;">Twój wynik: ---</div>
        </div>
    <?php } ?>



<?php include('../app/views/footer.php'); ?>