<?php include('../app/views/header.php'); ?>

<div class="page-header">
    <h1>Dostęp do materiałów mają tylko osoby zalogowane</h1>
</div>

<div class="loginFormWrap">
    <?php include('../app/views/loginForm.php'); ?>
</div>

<?php include('../app/views/footer.php'); ?>