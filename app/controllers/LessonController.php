<?php

class LessonController extends Controller {

    function indexAction() {
        if($_POST) {
            die('Niestety funcjonalnosc logowania nie zostala wdrozona');
        }
    }

    function tryAction() {
        $this->lesson=new Lekcja(1);
        $blok=new Blok;
        $bloki=$blok->readByLesson($this->lesson->getId());
        $this->bloki=array();
        foreach($bloki AS $blok) {
            $newBlok=new Blok;
            $newBlok->loadFromArray($blok);
            $this->bloki[]=$newBlok;
        }
    }

    function ajaxAction() {
        if(empty($_GET['command'])||empty($_GET['block'])||!is_numeric($_GET['block'])) exit;

        if($_GET['command']=='blockShow') {
            $blok=new Blok($_GET['block']);
            $blok->getParsedValue();
            echo json_encode($blok->getAnswers());
        }

        exit;
    }

}