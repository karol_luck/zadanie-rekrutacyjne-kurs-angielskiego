<?php

class Controller {
    public function printView($controller, $action) {
        $this->_controller=$controller;
        $this->_action=$action;
        include('../app/views/'.strtolower($controller).'/'.strtolower($action).'.php');
    }
}