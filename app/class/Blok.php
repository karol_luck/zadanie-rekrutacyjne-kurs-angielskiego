<?php

class Blok extends Model {

    protected $lekcja;
    protected $pozycja;
    protected $tresc;
    protected $dbVariables=array('lekcja', 'pozycja', 'tresc');

    protected $regex='/%[a-z]=[^%]{1,255}%/';
    protected $maxAnswerLength=1;
    protected $inputWidth=10;
    protected $answers=array();

    /**
     * @param $lesson int Id lekcji
     * @return array Zwraca listę bloków przypisanych do wybranej lekcji
     */
    function readByLesson($lesson) {
        $st=$this->pdo->prepare('SELECT * FROM blok WHERE lekcja = ? ORDER BY pozycja ASC');
        $st->execute(array($lesson));
        return $st->fetchAll();
    }

    /**
     * Zwraca gotowy tekst do wyświetlenia
     * (m.in. przekształca specjalne tagi w odtwarzacze MP3)
     */
    function getParsedValue() {
        $prepare = preg_replace_callback($this->regex, 'self::parseEntries', $this->get('tresc'));
        return $this->bindMaxVars($prepare);
    }

    public function hasAnswers() {
        return !empty($this->answers);
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    protected function parseEntries($matches) {
        $toParse=substr($matches[0], 1, -1); // Pomiń znaki specjalne
        $flag=substr($toParse, 0, 1); // Pobierz pierwszy znak (flagę)
        $value=substr($toParse, 2); // Pobierz wartość

        if($flag=='a') { // Wstaw audio
            $this->param_id=(empty($this->param_id)?1:++$this->param_id);
            $this->param_no=$this->id.'_'.$this->param_id;
            $this->param_url=$this->getSpeechUrl($value);
            ob_start();
            include('../app/views/audio.php');
            $contents = ob_get_contents();
            ob_end_clean();
            return $contents;
        }
        else if($flag=='o') { // Pole do wpisania odpowiedzi
            $this->answers[]=$value;
            if($this->maxAnswerLength<strlen($value)) $this->maxAnswerLength=strlen($value);
            return '<input type="text" class="answer" maxlength="%=maxlength%" style="width: %=maxwidth%px;" />';
        }
    }

    protected function bindMaxVars($text) {
        return str_replace('%=maxlength%', $this->maxAnswerLength,
            str_replace('%=maxwidth%', $this->inputWidth+8*$this->maxAnswerLength, $text)
        );
    }

    protected function getSpeechUrl($text) {
        //return 'http://translate.google.com/translate_tts?tl=en&q='.urlencode($text);
        return 'http://tts-api.com/tts.mp3?q='.urlencode($text);
    }

}