<?php

/**
 * Obiekty dziedziczą po tej klasie obsługę bazy danych
 */
abstract class Model {

    protected $id;
    protected $pdo;
    protected $className;
    protected $dbVariables=array();

    function __construct($id=false) {
        global $pdo;
        if(empty($pdo)) { // Jeżeli połączenie z bazą nie zostało jeszcze nawiązane
            $db=array();
            require('../app/config.php');
            try {
                $pdo=new PDO ( $db['type'].':dbname='.$db['name'].';host='.$db['host'].';charset=utf8', $db['user'], $db['password'] ) ;
            }
            catch (Exception $e) {
                die('Blad polaczenia z baza danych');
            }
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); // Domyślnie PDO będzie wybierał rekordy jako tablice asocjacyjne
        }
        $this->pdo=$pdo;
        $this->className=strtolower(get_class($this));

        if($id!==false && is_numeric($id) && $id>0) { // Jeżeli klasa została wywołana z parametrem "ID", oznacza, że obiekt ma być od razu zainicjowany (wczytany z bazy danych)
            $this->id=$id;
            if(!$this->read()) die('Error! #'.$this->id.' of '.$this->className.' not found!');
        }
    }

    /**
     * Inicjalizuje obiekt z tablicy asocjacynej
     * @param $array
     */
    function loadFromArray($array) {
        foreach($array AS $key=>$value) {
            if($key=='id') $this->id=$value;
            else $this->set($key, $value);
        }
    }

    /**
     * @return string Zwraca nazwę klasy danego obiektu
     */
    function getClassName() {
        return $this->className;
    }

    /**
     * @return int Zwraca id obiektu
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param $variable string Nazwa zwracanego atrybutu
     * @return Zwraca podany atrybut
     */
    public function get($variable) {
        if(property_exists($this, $variable) && in_array($variable, $this->dbVariables)) {
            return $this->$variable;
        }
        return null;
    }

    /**
     * @param $variable string Nazwa ustawianego atrybutu
     * @param $value string Wartość ustawianego atrybutu
     */
    public function set($variable, $value) {
        if(property_exists($this, $variable) && in_array($variable, $this->dbVariables)) {
            $this->$variable=$value;
        }
    }

    /** OPERACJE CRUD */

    public function create() {
        $attributes=array();
        $insertQuery='';
        $valuesQuery='';
        foreach($this->dbVariables AS $dbVariable) {
            $insertQuery.=" `$dbVariable` ,";
            $valuesQuery.=" ? ,";
            $attributes[]=$this->$dbVariable;
        }
        $insertQuery=substr($insertQuery, 0, -1);
        $valuesQuery=substr($valuesQuery, 0, -1);
        $st=$this->pdo->prepare('INSERT INTO '.$this->getClassName().' ('.$insertQuery.') VALUES ('.$valuesQuery.')');
        $st->execute($attributes);
    }

    public function read() {
        $st=$this->pdo->prepare('SELECT * FROM '.$this->getClassName().' WHERE id = ?');
        $st->execute(array($this->getId()));
        $result=$st->fetch();
        if(empty($result)) return false;
        foreach($result AS $key=>$value) {
            if($key=='id') $this->id=$value;
            else $this->set($key, $value);
        }
        return true;
    }

    public function update() {
        $attributes=array();
        $updateQuery='';
        foreach($this->dbVariables AS $dbVariable) {
            $updateQuery.=" `$dbVariable` = ? ,";
            $attributes[]=$this->$dbVariable;
        }
        $updateQuery=substr($updateQuery, 0, -1);
        $st=$this->pdo->prepare('UPDATE '.$this->getClassName().' SET '.$updateQuery.' WHERE id = ?');
        $attributes[]=$this->getId();
        $st->execute($attributes);
    }

    public function delete() {
        $st=$this->pdo->prepare('DELETE FROM '.$this->getClassName().' WHERE id = ?');
        $st->execute(array($this->getId()));
    }

    public function readAll() {
        $st=$this->pdo->query('SELECT * FROM '.$this->getClassName());
        $st->execute();
        return $st->fetchAll();
    }

}